#ifndef AST_FWD_H
#define AST_FWD_H

#include <memory>

class Expression;
class TopLevel;
class Chunk;

typedef std::shared_ptr<Expression> ExpressionPtr;
typedef std::shared_ptr<TopLevel> TopLevelPtr;

// Thin wrapper around std::shared_ptr<Chunk> to allow to automated conversion
// from ExpressionPtr
struct ChunkPtr : public std::shared_ptr<Chunk>
{
  typedef std::shared_ptr<Chunk> Base;

  ChunkPtr()
    : Base()
  { }
  ChunkPtr(const std::shared_ptr<Chunk>& o)
    : Base(o)
  { }

  template <typename T>
  ChunkPtr(const std::shared_ptr<T>& e);

  ChunkPtr(const ChunkPtr&) = default;
  ChunkPtr(ChunkPtr&&) = default;

  ChunkPtr& operator=(const ChunkPtr&) = default;
  ChunkPtr& operator=(ChunkPtr&&) = default;
};


#endif // AST_FWD_H

