#ifndef LEXER_H
#define LEXER_H

#include <string>
#include "parser.tab.hpp"
#include "ast.h"
#include <QFile>
#include <QTextStream>
#include <QHash>
#include <QSet>
#include <QRegExp>
#include <QString>
#include <QList>
#include <utility>

#define YY_DECL yy::Parser::symbol_type yylex(Lexer& lexer)
class Interpreter;

class Lexer
{
public:
  Lexer(Interpreter& interpreter, bool trace_lexing = false, bool trace_parsing = false);
  ~Lexer();

  int parse(const QString& f);

  bool trace_lexing, trace_parsing;

  yy::Parser::symbol_type next();

  void error(const yy::location& l, const std::string& m);
  void error(const std::string& m);

  Interpreter& interpreter;

  std::string file;
  QString filename;

  bool isInteractive() const { return _interactive; }
  void newStatement();

private:

  int scan_begin();
  void scan_end();

  QString parseString();
  QString parseAtom();
  yy::Parser::symbol_type makeAtom(const QString& atom) const;
  bool atEnd(int offset = 0);
  bool advance();
  bool skip(QString toEnd);
  QChar currentChar() const;
  QChar lookAhead(int n = 1);
  // Return true unless we reach the end of the file
  bool skipSpaces();

  void movePosition(const QStringRef& str);

  yy::location loc;
  yy::position pos;

  QString buffer;
  int buf_pos;
  bool _interactive;
  bool _newStatement;

  QString readFixedSize(qint64 min);
  QString readLine(qint64 min);

  QString (Lexer::*readBuffer)(qint64 mint);

  QString errorMessage;
  QFile f;
  QTextStream ts;
  static QHash<QString, yy::Parser::token::yytokentype> keywords;
  static QSet<QChar> delimiters;
};

YY_DECL;

#endif // LEXER_H

