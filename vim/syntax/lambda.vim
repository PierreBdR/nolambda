if exists("b:current_syntax")
  finish
endif

syn keyword nolTopLevel define
syn keyword nolKeywords λ lambda fail ⊥

syn region nolCommentL start="--" end="$" keepend
syn region nolComment start="{-" end="-}"

syn match nolLambdaArg "(\s*\(lambda\|λ\)\s[^.()]*\." contains=nolKeywords

syn match nolLambdaVars "[^().]+\." contained

syn match nolDefine "(\s*define\s\+[^.() \t\n]\+" contains=nolTopLevel

syn match nolInclude "\v#include"

syn region nolString start=+"+ skip=+\\\\\|\\"+ end=+"+


command -nargs=+ HiLink hi def link <args>

HiLink nolTopLevel Include
HiLink nolInclude Include
HiLink nolKeywords Keyword
HiLink nolComment Comment
HiLink nolCommentL Comment
HiLink nolLambdaArg Number
HiLink nolDefine Identifier
HiLink nolString String

delcommand HiLink
