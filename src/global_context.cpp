#include "global_context.h"

ChunkPtr GlobalContext::find(QString name) const
{
  auto found = _dict.find(name);
  if(found == _dict.end())
    return ChunkPtr();
  return found->second;
}

ContextPtr GlobalContext::set(QString name, ChunkPtr value)
{
  _dict[name] = value;
  return this->shared_from_this();
}

bool GlobalContext::update(QString name, ChunkPtr value)
{
  auto found = _dict.find(name);
  if(found == _dict.end())
    return false;
  found->second = value;
  return true;
}

std::vector<std::pair<QString,ChunkPtr> > GlobalContext::items() const
{
  typedef std::pair<QString,ChunkPtr> item_type;
  auto res = std::vector<item_type>();
  res.reserve(_dict.size());
  for(auto p: _dict)
    res.push_back(p);
  std::sort(res.begin(), res.end(),
            [](const item_type& i1, const item_type& i2) { return i1.first < i2.first; });
  return res;
}

std::vector<QString> GlobalContext::vars() const
{
  auto res = std::vector<QString>();
  res.reserve(_dict.size());
  for(auto p: _dict)
    res.push_back(p.first);
  std::sort(res.begin(), res.end());
  return res;
}

bool GlobalContext::empty() const
{
  return _dict.empty();
}

