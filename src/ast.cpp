#include "ast.h"
#include "global.h"
#include <QStringList>
#include <QRegularExpression>
#include "local_context.h"
#include "interpreter.h"

namespace
{
QRegularExpression complexAtom = QRegularExpression("[.(){} \\r\\n]");
}

QString escape_atom(QString str)
{
  if(str.contains(complexAtom))
  {
    str.replace("\n", "\\n");
    str.replace("\t", "\\t");
    str.replace("\r", "\\r");
    return QString("\"%1\"").arg(str);
  }
  return str;
}

QTextStream& operator<<(QTextStream& ts, const TopLevel& tl)
{
  return ts << tl.show();
}

QTextStream& operator<<(QTextStream& ts, const Expression& e)
{
  return ts << e.show();
}

QTextStream& operator<<(QTextStream& ts, const Chunk& e)
{
  return ts << e.show();
}

Define::Define(const QString& name, const ExpressionPtr& value)
  : _name(name)
  , _value(value)
{
  if(_name.isEmpty())
  {
    err << "Error, trying top create a definition with an empty name." << endl;
    throw BadTopLevel();
  }
  if(not _value)
  {
    err << "Error, trying to create a definition with a NULL value." << endl;
    throw BadTopLevel();
  }
}

QString Define::show() const
{
  return QString::fromUtf8(u8"%1 → %2").arg(name()).arg(value()->show());
}

ExpressionPtr Define::eval(Interpreter& interpreter) const
{
  ExpressionPtr val = value();
  interpreter.global()->set(name(), chunk(val, localContext()));
  return val;
}

Expr::Expr(const ExpressionPtr& expr)
  : _expr(expr)
{
  if(not _expr)
  {
    err << "Error, trying to create a top-level expression with a NULL value." << endl;
    throw BadTopLevel();
  }
}

QString Expr::show() const
{
  return expr()->show();
}

ExpressionPtr Expr::eval(Interpreter& interpreter) const
{
  return interpreter.simplify(chunk(expr(), localContext()));
}

Include::Include(const QString& filename)
  : _filename(filename)
{
  if(_filename.isEmpty())
  {
    err << "Error, trying to create an inclusion with an empty file name." << endl;
    throw BadTopLevel();
  }
}

QString Include::show() const
{
  return QString("#include %1").arg(escape_atom(filename()));
}

ExpressionPtr Include::eval(Interpreter& interpreter) const
{
  if(interpreter.run(filename()))
    return ExpressionPtr();
  return atom(filename());
}

ChunkPtr Expression::eval(Interpreter& , const ContextPtr& local)
{
  return chunk(this->shared_from_this(), local);
}

Chunk::Chunk(const Chunk& copy, const ContextPtr& current)
  : _value(copy.value())
  , _local(copy.local() ? copy.local() : current)
  , _evaluated(copy._evaluated)
{ }

Chunk::Chunk(const ExpressionPtr& expr, const ContextPtr& local, bool evaluated)
  : _value(expr)
  , _local(local)
  , _evaluated(evaluated)
{ }

Chunk::Chunk(const ExpressionPtr& expr, bool evaluated)
  : _value(expr)
  , _local(ContextPtr())
  , _evaluated(evaluated)
{ }

QString Chunk::show() const
{
  QString base = value()->show();
  QString context;
  if(local())
  {
    QTextStream ts(&context);
    ts << local();
  }
  else
    context = QString::fromUtf8(u8"Ø");
  return QString("%1 // %2").arg(base).arg(context);
}

ChunkPtr chunk(ChunkPtr c, ContextPtr local)
{
  if(c->local() or not local)
    return c;
  return chunk(*c, local);
}

ChunkPtr Chunk::eval(Interpreter& interpreter)
{
  //Indentation ind(interpreter);
  //if(interpreter.trace_eval)
    //err << ind << "C." << *this << endl;
  auto result = value()->eval(interpreter, local());
  _local = result->local();
  _value = result->value();
  //if(interpreter.trace_eval)
    //err << ind << "C#" << *this << endl;
  return this->shared_from_this();
}

ContextPtr makeLocalContext(ChunkPtr l)
{
  if(l->local())
    return l->local();
  return localContext();
}

Atom::Atom(const QString& name)
  : Expression()
  , _name(name)
{
  if(_name.isEmpty())
  {
    err << "Error, creating atom with an empty name" << endl;
    throw BadExpression();
  }
}

QString Atom::show(bool) const
{
  return escape_atom(name());
}

ChunkPtr Atom::eval(Interpreter& interpreter, const ContextPtr& local)
{
  Indentation ind(interpreter);
  if(interpreter.trace_eval)
    err << ind << "a. " << name() << endl;
  auto result = interpreter.resolve(name(), local);
  if(interpreter.trace_eval)
    err << ind << "a# " << result->show() << endl;
  return result;
}

Lambda::Lambda(const QString& var, const ChunkPtr& body)
  : Expression()
  , _var(var)
  , _body(body)
{
  if(_var.isEmpty())
  {
    err << "Error, creating lambda expression with an empty variable" << endl;
    throw BadExpression();
  }
  if(not _body)
  {
    err << "Error, createing lambda expression with NULL body" << endl;
    throw BadExpression();
  }
}

QString Lambda::show(bool) const
{
  return QString::fromUtf8(u8"(λ %1)").arg(showInternal());
}

QString Lambda::showInternal() const
{
  auto b = body()->value();
  auto l = std::dynamic_pointer_cast<Lambda>(b);
  if(l)
    return QString("%1 %2").arg(var()).arg(l->showInternal());
  return QString("%1 . %2").arg(var()).arg(b->show(false));
}

LambdaPtr makeLambda(QStringList vars, const ExpressionPtr& body)
{
  if(vars.empty()) return LambdaPtr(); // error
  auto res = lambda(vars.back(), body);
  vars.pop_back();
  while(not vars.empty())
  {
    res = lambda(vars.back(), res);
    vars.pop_back();
  }
  return res;
}

App::App(const ChunkPtr& fct, const ChunkPtr& arg)
  : Expression()
  , _fct(fct)
  , _arg(arg)
{
  if(not _fct)
  {
    err << "Error, creating function application with NULL function" << endl;
    throw BadExpression();
  }
  if(not arg)
  {
    err << "Error, creating function application with NULL argument" << endl;
    throw BadExpression();
  }
}

QString App::show(bool needBracket) const
{
  auto in = QString("%1 %2").arg(fct()->value()->show(false)).arg(arg()->value()->show(true));
  if(needBracket)
    return QString("(%1)").arg(in);
  return in;
}

ChunkPtr App::eval(Interpreter& interpreter, const ContextPtr& local)
{
  Indentation ind(interpreter);
  auto fct = chunk(_fct, local);
  auto arg = chunk(_arg, local);
  if(interpreter.trace_eval)
    err << ind << "@." << fct << " @ " << arg << endl;
  ChunkPtr f = chunk(fct->eval(interpreter), local);
  auto l = std::dynamic_pointer_cast<Lambda>(f->value());
  // If the function simplifies to a lambda-expression
  // we evaluate it
  if(l)
  {
    QString var = l->var();
    ChunkPtr body = l->body();
    ContextPtr new_local = f->local()->set(var, arg);
    ChunkPtr new_expr = chunk(*body, new_local);
    if(interpreter.trace_eval)
      err << ind
          << QString::fromUtf8(u8"@λ.") << var
          << QString::fromUtf8(u8" → ") << body
          << " // " << new_local << endl;
    auto res = new_expr->eval(interpreter);
    if(interpreter.trace_eval)
      err << ind
          << QString::fromUtf8(u8"#λ.") << res << " // " << res->local() << endl;
    return res;
  }
  auto intern = std::dynamic_pointer_cast<Internal>(f->value());
  if(intern)
  {
    if(interpreter.trace_eval)
      err << ind
          << QString::fromUtf8(u8"@¤.") << intern
          << " @ " << arg << endl;
    auto expr = intern->eval(interpreter, arg);
    if(interpreter.trace_eval)
      err << ind
          << QString::fromUtf8(u8"#¤.") << expr
          << endl;
    return expr;
  }
  if(interpreter.trace_eval)
    err << ind
        << "@@." << f
        << " @ " << arg << endl;
  // Otherwise, we evaluate the argument
  return chunk(app(f, interpreter.simplify(arg)), local);
}


QString Fail::show(bool) const
{
  return QString::fromUtf8(u8"⊥");
}

ChunkPtr Fail::eval(Interpreter& , const ContextPtr&)
{
  throw FailComputation();
}

Internal::Internal(const QString& name)
  : Expression()
  , _name(name)
{ }

QString Internal::show(bool) const
{
  return QString::fromUtf8(u8"¤%1").arg(_name);
}

