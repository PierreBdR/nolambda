#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "global.h"
#include "context_fwd.h"
#include "ast_fwd.h"
#include <string>
#include <iostream>
#include <QDir>
#include <exception>

struct UnknownVariable : public std::exception
{
  const char* what() const noexcept override
  {
    return "Trying to access an unknown variable";
  }
};

class Lexer;

namespace yy
{
class location;
class position;
}

class Interpreter
{
public:
  enum ERROR
  {
    NO_ERROR = 0,
    SYNTAX_ERROR,
    NO_SUCH_FILE,
    CANNOT_READ_STDIN
  };
  Interpreter();

  ERROR run(const QString& filename = QString());

  bool trace_parsing;
  bool trace_lexing;
  bool trace_eval;

  Interpreter& operator<<(TopLevelPtr cmd);

  void setInteractive(bool value);

  ContextPtr global() { return _global; }

  QDir currentDir();

  ChunkPtr resolve(QString name, ContextPtr local = ContextPtr());

  QString nextVar();

  ExpressionPtr simplify(ChunkPtr value);

  void add_indent() { ++_indentation; }
  void rem_indent() { --_indentation; }
  int indentLevel() { return _indentation; }
  QString indent() { return QString(2*_indentation, ' '); }

private:
  bool _interactive;
  ContextPtr _global;
  QDir _currentDir;
  QString varName;
  int _indentation = 0;
};

struct Indentation
{
  Indentation(Interpreter& i)
    : interpreter(i)
  {
    interpreter.add_indent();
  }

  ~Indentation()
  {
    interpreter.rem_indent();
  }

  Interpreter& interpreter;
};

inline QTextStream& operator<<(QTextStream& ts, const Indentation& id)
{
  return ts << QString(2*id.interpreter.indentLevel(), ' ');
}
template <typename Char, typename Traits>
std::basic_ostream<Char,Traits>& operator<<(std::basic_ostream<Char,Traits>& ss, const Indentation& id)
{
  return ss << std::basic_string<Char, Traits>(2*id.interpreter.indentLevel(), ' ');
}

QTextStream& operator<<(QTextStream& ts, const yy::location& pos);
QTextStream& operator<<(QTextStream& ts, const yy::position& pos);
QTextStream& operator<<(QTextStream& ts, const QStringList& sl);

template <typename Char, typename Traits>
std::basic_ostream<Char,Traits>& operator<<(std::basic_ostream<Char,Traits>& ss, const QStringList& sl)
{
  QString str;
  QTextStream ts(&str);
  ts << sl;
  return ss << str.toLocal8Bit().data();
}

template <typename Char, typename Traits>
std::basic_ostream<Char,Traits>& operator<<(std::basic_ostream<Char,Traits>& ss, const QString& str)
{
  return ss << str.toLocal8Bit().data();
}

#endif // INTERPRETER_H

