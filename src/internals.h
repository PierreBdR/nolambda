#ifndef INTERNALS_H
#define INTERNALS_H

#include "ast.h"

ContextPtr setupInternals(ContextPtr context);

#endif // INTERNALS_H

