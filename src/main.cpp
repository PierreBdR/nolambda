#include <QString>
#include <QTextStream>
#include <stdlib.h>
#include <QTextCodec>
#include "lexer.h"
#include <QCoreApplication>
#include <QStringList>
#include <QCommandLineParser>
#include <QCommandLineOption>

#include "ast.h"
#include "interpreter.h"
#include "context.h"

QTextStream out(stdout);
QTextStream err(stderr);

int main(int argc, char *argv[])
{
  QCoreApplication::setApplicationName("EvaLambda");
  QCoreApplication::setApplicationVersion("0.1");
  QCoreApplication::setOrganizationDomain("barbierdereuille.net");
  QCoreApplication::setOrganizationName("Pierre Barbier de Reuille");

  QCoreApplication app(argc, argv);
  out.setCodec("utf-8");
  err.setCodec("utf-8");

  QCommandLineParser cmd;
  cmd.addHelpOption();
  QCommandLineOption traceLexer(QStringList() << "l" << "lexer", "Trace the lexer function.");
  cmd.addOption(traceLexer);
  QCommandLineOption traceParser(QStringList() << "p" << "parser", "Trace the parser function.");
  cmd.addOption(traceParser);
  QCommandLineOption traceEval(QStringList() << "e" << "eval", "Trace the expression evaluation.");
  cmd.addOption(traceEval);
  cmd.addPositionalArgument("filename", "Name of the file to parse.");

  cmd.process(app);

  Interpreter interpreter;

  interpreter.trace_parsing = cmd.isSet(traceParser);
  interpreter.trace_lexing = cmd.isSet(traceLexer);
  interpreter.trace_eval = cmd.isSet(traceEval);

  const QStringList& args = cmd.positionalArguments();

  int res = interpreter.run(args.empty() ? QString() : args.at(0));

  if(res == 0)
  {
    out << "Latest known global context=\n" << *interpreter.global() << endl;
  }

  return res;
}
