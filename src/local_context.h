#ifndef LOCAL_CONTEXT_H
#define LOCAL_CONTEXT_H

#include "context.h"

class LocalContext : public Context, public std::enable_shared_from_this<LocalContext>
{
public:
  typedef std::shared_ptr<LocalContext> LocalContextPtr;

  LocalContext();
  LocalContext(QString name, ChunkPtr expr, LocalContextPtr next);
  LocalContext(const LocalContext&) = default;
  LocalContext(LocalContext&&) = default;

  ChunkPtr find(QString name) const override;
  ContextPtr set(QString name, ChunkPtr value) override;
  std::vector<std::pair<QString,ChunkPtr> > items() const override;
  std::vector<QString> vars() const override;
  bool update(QString name, ChunkPtr new_value) override;
  bool empty() const override;

  QString name() const { return _name; }
  ChunkPtr value() const { return _value; }
  LocalContextPtr next() const { return _next; }

private:
  LocalContextPtr _next;
  QString _name;
  ChunkPtr _value;
};

MAKE_PTR(LocalContext, localContext)
MAKE_CPTR(LocalContext, clocalContext)

#endif // LOCAL_CONTEXT_H

