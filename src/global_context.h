#ifndef GLOBAL_CONTEXT_H
#define GLOBAL_CONTEXT_H

#include "context.h"

class GlobalContext : public Context, public std::enable_shared_from_this<GlobalContext>
{
public:
  ChunkPtr find(QString name) const override;
  ContextPtr set(QString name, ChunkPtr value) override;
  std::vector<std::pair<QString,ChunkPtr> > items() const override;
  std::vector<QString> vars() const override;
  bool update(QString name, ChunkPtr value) override;
  bool empty() const override;

private:
  std::unordered_map<QString, ChunkPtr> _dict;
};

MAKE_PTR(GlobalContext, globalContext)


#endif // GLOBAL_CONTEXT_H

