#include "internals.h"
#include "local_context.h"
#include <QSet>

class FromNumber : public Internal
{
public:
  FromNumber()
    : Internal("fromNumber")
  { }
  FromNumber(const FromNumber&) = default;
  FromNumber(FromNumber&&) = default;
  ChunkPtr eval(Interpreter& interpreter, const ChunkPtr& arg) override
  {
    auto a = std::dynamic_pointer_cast<Atom>(arg->value());
    if(a)
    {
      bool ok;
      int n = a->name().toInt(&ok);
      if(!ok or n < 0)
      {
        err << "Error, fromNumber internal requires an atom representing a natural number, got " << a << endl;
        throw FailComputation();
      }
      auto body = chunk(atom("x"));
      for(int i = 0 ; i < n ; i++)
        body = app(atom("p"), body);
      return lambda("p", lambda("x", body));
    }
    err << "Error, fromNumber internal requires an atom representing a natural number, got " << arg << endl;
    throw FailComputation();
  }

  EXPRESSION_COPY(FromNumber)
};

MAKE_PTR(FromNumber, fromNumber)

class Number : public Expression
{
public:
  explicit Number(int n)
    : Expression()
    , _num(n)
  { }

  Number(const Number& copy) = default;
  Number(Number&&) = default;

  int value() const { return _num; }

  QString show(bool needBracket = true) const override
  {
    return QString::fromUtf8(u8"#%1").arg(_num);
  }

  EXPRESSION_COPY(Number)

private:
  int _num;
};

MAKE_PTR(Number, number)

class Increment : public Internal
{
public:
  Increment() : Internal("increment") { }
  Increment(const Increment&) = default;
  Increment(Increment&&) = default;

  ChunkPtr eval(Interpreter& interpreter, const ChunkPtr& arg) override
  {
    auto e = arg->eval(interpreter);
    auto num = std::dynamic_pointer_cast<Number>(e->value());
    if(num)
      return number(num->value()+1);
    err << "Error, internal function Increment requires a number as argument" << endl;
    throw FailComputation();
  }

  EXPRESSION_COPY(Increment)
};

MAKE_PTR(Increment, increment)

class ToNumber : public Internal
{
public:
  ToNumber()
    : Internal("toNumber")
  { }
  ToNumber(const ToNumber&) = default;
  ToNumber(ToNumber&&) = default;
  ChunkPtr eval(Interpreter& interpreter, const ChunkPtr& arg) override
  {
    auto e = arg->eval(interpreter);
    auto conv = chunk(app(app(e, increment()), number(0)));
    return conv->eval(interpreter);
  }

  EXPRESSION_COPY(ToNumber)
};

MAKE_PTR(ToNumber, toNumber)

namespace
{
QSet<QString> build_falsehood()
{
  QSet<QString> res;
  res << "f" << "n" << "false" << "no" << "0";
  return res;
}

const QSet<QString> falsehood = build_falsehood();
}

class FromBool : public Internal
{
public:
  FromBool()
    : Internal("fromBool")
  { }
  FromBool(const FromBool&) = default;
  FromBool(FromBool&&) = default;
  ChunkPtr eval(Interpreter& interpreter, const ChunkPtr& arg) override
  {
    auto a = std::dynamic_pointer_cast<Atom>(arg->value());
    if(a)
    {
      bool ok;
      QString val = a->name().toLower();
      if(falsehood.contains(val))
        return lambda("t", lambda("f", atom("f")));
      return lambda("t", lambda("f", atom("t")));
    }
    err << "Error, fromBool internal requires an atom representing a natural number, got " << arg << endl;
    throw FailComputation();
  }

  EXPRESSION_COPY(FromBool)
};

MAKE_PTR(FromBool, fromBool)

class Bool : public Expression
{
public:
  explicit Bool(bool n)
    : Expression()
    , _val(n)
  { }

  Bool(const Bool& copy) = default;
  Bool(Bool&&) = default;

  bool value() const { return _val; }

  QString show(bool needBracket = true) const override
  {
    if(_val)
      return "#true";
    else
      return "#false";
  }

  EXPRESSION_COPY(Bool)

private:
  bool _val;
};

MAKE_PTR(Bool, boolean)

class ToBool : public Internal
{
public:
  ToBool()
    : Internal("toBool")
  { }
  ToBool(const ToBool&) = default;
  ToBool(ToBool&&) = default;
  ChunkPtr eval(Interpreter& interpreter, const ChunkPtr& arg) override
  {
    auto e = arg->eval(interpreter);
    auto conv = chunk(app(app(e, boolean(true)), boolean(false)));
    return conv->eval(interpreter);
  }

  EXPRESSION_COPY(ToBool)
};

MAKE_PTR(ToBool, toBool)


ContextPtr setupInternals(ContextPtr context)
{
  context = context->set("#", chunk(fromNumber(), true));
  context = context->set("to#", chunk(toNumber(), true));
  context = context->set("?", chunk(fromBool(), true));
  context = context->set("to?", chunk(toBool(), true));
  return context;
}

