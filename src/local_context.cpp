#include "local_context.h"
#include <unordered_set>

LocalContext::LocalContext()
{ }

LocalContext::LocalContext(QString name, ChunkPtr value, LocalContextPtr next)
  : _next(next)
  , _name(name)
  , _value(value)
{ }

ChunkPtr LocalContext::find(QString name) const
{
  if(not _value) return ChunkPtr();
  if(_name == name) return _value;
  return _next->find(name);
}

bool LocalContext::empty() const
{
  return not _value;
}

bool LocalContext::update(QString name, ChunkPtr new_value)
{
  if(not _value) return false;
  if(_name == name)
  {
    _value = new_value;
    return true;
  }
  return _next->update(name, new_value);
}

ContextPtr LocalContext::set(QString name, ChunkPtr value)
{
  return localContext(name, value, this->shared_from_this());
}

std::vector<std::pair<QString,ChunkPtr> > LocalContext::items() const
{
  typedef std::pair<QString,ChunkPtr> item_type;
  std::unordered_set<QString> found;
  std::vector<item_type> result;
  LocalContextCPtr cur = this->shared_from_this();
  while(cur->value())
  {
    if(found.count(cur->name()) == 0)
    {
      result.push_back(std::make_pair(cur->name(), cur->value()));
      found.insert(cur->name());
    }
    cur = cur->next();
  }
  std::sort(result.begin(), result.end(),
            [](const item_type& i1, const item_type& i2) { return i1.first < i2.first; });
  return result;
}

std::vector<QString> LocalContext::vars() const
{
  std::unordered_set<QString> found;
  std::vector<QString> result;
  LocalContextCPtr cur = this->shared_from_this();
  while(cur->value())
  {
    if(found.count(cur->name()) == 0)
    {
      result.push_back(cur->name());
      found.insert(cur->name());
    }
    cur = cur->next();
  }
  std::sort(result.begin(), result.end());
  return result;
}

