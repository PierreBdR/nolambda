#include "context.h"
#include "ast.h"

QTextStream& operator<<(QTextStream& ts, const Context& context)
{
  ts << "{";
  for(auto p: context.items())
  {
    ts << "| " << p.first << QString::fromUtf8(u8" → ") << p.second->value();
    if(p.second->evaluated())
      ts << " !";
    else
      ts << " // " << p.second->local();
    ts << " |";
  }
  ts << "}";
  return ts;
}
