#ifndef GLOBAL_H
#define GLOBAL_H

#include <QTextStream>
#include <memory>
#include <iostream>

extern QTextStream out;
extern QTextStream err;

/**
 * \define MAKE_PTR
 * \param cls Type to define
 * \param fct Name of the constructor function
 *
 * Define the shared pointer to the type and a constructor function returning the shared pointer
 */
#define MAKE_PTR(cls, fct) \
  typedef std::shared_ptr<cls> cls##Ptr; \
  template <typename... T> \
  cls##Ptr fct(T&& ...args) \
  { return std::make_shared<cls>(std::forward<T>(args)...); }

/**
 * \define MAKE_CPTR
 * \param cls Type to define
 * \param fct Name of the constructor function
 *
 * Define the shared pointer to the constant type and a constructor function returning the shared pointer
 */
#define MAKE_CPTR(cls, fct) \
  typedef std::shared_ptr<const cls> cls##CPtr; \
  template <typename... T> \
  cls##CPtr fct(T&& ...args) \
  { return std::make_shared<const cls>(std::forward<T>(args)...); }

template <typename T>
QTextStream& operator<<(QTextStream& ts, const std::shared_ptr<T>& ptr)
{
  if(ptr) return ts << *ptr;
  else return ts << QString::fromUtf8(u8"Ø");
}

template <typename Char, typename Traits, typename T>
std::basic_ostream<Char,Traits>& operator<<(std::basic_ostream<Char,Traits>& ts, const std::shared_ptr<T>& ptr)
{
  if(ptr) return ts << *ptr;
  else return ts << u8"Ø";
}

#endif // GLOBAL_H

