#ifndef CONTEXT_H
#define CONTEXT_H

#include <memory>
#include <unordered_map>
#include <QString>
#include <QHash>

#include "global.h"
#include "context_fwd.h"
#include "ast_fwd.h"

namespace std
{
template <>
struct hash<QString>
{
  size_t operator()(const QString& s) const
  {
    return qHash(s);
  }
};
}

class Context
{
public:
  virtual ~Context() { }
  typedef std::shared_ptr<Context> ContextPtr;

  virtual ChunkPtr find(QString name) const = 0;
  virtual ContextPtr set(QString name, ChunkPtr value) = 0;
  virtual std::vector<std::pair<QString,ChunkPtr> > items() const = 0;
  virtual std::vector<QString> vars() const = 0;
  virtual bool update(QString name, ChunkPtr value) = 0;
  virtual bool empty() const = 0;
};
typedef std::shared_ptr<Context> ContextPtr;
typedef std::shared_ptr<const Context> ContextCPtr;

QTextStream& operator<<(QTextStream& ts, const Context& context);

template <typename Char, typename Traits>
std::basic_ostream<Char,Traits>& operator<<(std::basic_ostream<Char,Traits>& ss, const Context& sl)
{
  QString str;
  QTextStream ts(&str);
  ts << sl;
  return ss << str.toLocal8Bit().data();
}

#endif // CONTEXT_H

