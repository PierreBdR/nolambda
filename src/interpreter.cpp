#include "interpreter.h"
#include "parser.tab.hpp"
#include <sstream>
#include "lexer.h"
#include <stdio.h>
#include "global_context.h"
#include "internals.h"

QTextStream& operator<<(QTextStream& ts, const yy::position& pos)
{
  std::ostringstream ss;
  ss << pos;
  return ts << QString::fromUtf8(ss.str().c_str());
}

QTextStream& operator<<(QTextStream& ts, const yy::location& pos)
{
  std::ostringstream ss;
  ss << pos;
  return ts << QString::fromUtf8(ss.str().c_str());
}

QTextStream& operator<<(QTextStream& ts, const QStringList& sl)
{
  return ts << "[" << sl.join(", ") << "]";
}

Interpreter::Interpreter()
  : _global(setupInternals(globalContext()))
  , _currentDir(QDir::current())
{
}

Interpreter::ERROR Interpreter::run(const QString& filename)
{
  QDir prevDir = _currentDir;
  QString filePath = filename;
  if(not filePath.isEmpty())
  {
    filePath = QDir::cleanPath(_currentDir.absoluteFilePath(filename));
    _currentDir = QFileInfo(filePath).absoluteDir();
  }
  Lexer lexer(*this, trace_lexing, trace_parsing);
  ERROR result = (ERROR)lexer.parse(filePath);
  _currentDir = prevDir;
  return result;
}

void Interpreter::setInteractive(bool value)
{
  _interactive = value;
}

QString Interpreter::nextVar()
{
  for(int pos = 0 ; pos < varName.size() ; pos++)
  {
    auto c = varName[pos];
    if(c >= 'a' and c <= 'z') // touch only characters between 'a' and 'z'
    {
      if(c < 'z')
      {
        c = QChar(c.unicode()+1);
        return varName;
      }
      c = 'a';
    }
  }
  varName.push_back('a');
  return varName;
}

Interpreter& Interpreter::operator<<(TopLevelPtr cmd)
{
  if(not _interactive)
    out << ">>> " << cmd << endl;
  try
  {
    varName = "";
    ExpressionPtr res = cmd->eval(*this);
    out << res << endl;
  }
  catch(const std::exception& ex)
  {
    err << "Error during computation: " << ex.what() << endl;
  }
  return *this;
}

ChunkPtr Interpreter::resolve(QString name, ContextPtr local)
{
  Indentation ind(*this);
  ChunkPtr e;
  bool is_local = false;
  if(local)
  {
    e = local->find(name);
    if(e)
      is_local = true;
  }
  if(not e)
    e = global()->find(name);
  if(not e)
  {
    err << "Error, no variable called " << escape_atom(name) << " in either the local or global context" << endl;
    throw UnknownVariable();
  }

  if(trace_eval)
    err << ind << "r. " << e << " -- is local = " << is_local << " -- is evaluated = " << e->evaluated() << endl;

  if(e->evaluated())
    return e;

  ChunkPtr ne = e->eval(*this);


  if(trace_eval)
    err << ind << "r# " << ne->show() << endl;

  if(is_local)
    local->update(name, ne);
  else
    global()->update(name, ne);
  return ne;
}

ExpressionPtr Interpreter::simplify(ChunkPtr value)
{
  Indentation ind(*this);
  // First, evaluate the expression
  if(trace_eval)
    err << ind
        << "Evaluating: " << value->show() << endl;
  auto e = value->eval(*this);
  if(trace_eval)
    err << ind << "Evaluated into: " << e->show() << endl;
  auto l = std::dynamic_pointer_cast<Lambda>(e->value());
  if(l) // If the result of the evaluation is a lambda expression
  {
    auto var = l->var();
    auto body = l->body();
    auto unique_var = nextVar();
    auto unique_atom = chunk(atom(unique_var), true);
    ContextPtr new_context = makeLocalContext(e)->set(var, unique_atom);
    if(trace_eval)
      err << ind
          <<  "Setting variable " << var << " to atom " << unique_atom << endl;
    body = chunk(body, new_context);
    auto new_body = simplify(body);
    return lambda(unique_var, new_body);
  }
  if(trace_eval)
    err << ind << "This wasn't a lambda expression" << endl;
  return e->value();
}

