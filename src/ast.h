#ifndef AST_H
#define AST_H

#include <QTextStream>
#include <QString>
#include <iostream>
#include <exception>
#include "global.h"

#include "ast_fwd.h"
#include "context_fwd.h"

class Interpreter;

struct BadExpression : public std::exception
{
  const char* what() const noexcept override {
    return "Creating bad expression";
  }
};

struct BadTopLevel : public std::exception
{
  const char* what() const noexcept override {
    return "Creating bad top level command";
  }
};

class TopLevel
{
public:
  virtual ~TopLevel() { }
  virtual QString show() const = 0;
  virtual TopLevelPtr copy() const = 0;
  virtual ExpressionPtr eval(Interpreter& interpreter) const = 0;
};

#define TOP_LEVEL_COPY(cls) \
  std::shared_ptr<TopLevel> copy() const override \
  { return std::make_shared<cls>(*this); }

QTextStream& operator<<(QTextStream& ts, const TopLevel& tl);
class Chunk : public std::enable_shared_from_this<Chunk>
{
public:
  Chunk(const ExpressionPtr& expr, const ContextPtr& local = ContextPtr(), bool evaluated = false);
  /**
   * Create a chunk without context.
   */
  Chunk(const ExpressionPtr& expr, bool evaluated);
  /**
   * Copy constructor
   *
   * If the current context is specified, and if the current chunk doesn't have a context,
   * the new chunk will have the current context
   */
  Chunk(const Chunk& copy, const ContextPtr& current = ContextPtr());
  Chunk(Chunk&&) = default;

  ContextPtr local() const { return _local; }
  ExpressionPtr value() const { return _value; }

  ChunkPtr eval(Interpreter& interpreter);

  bool evaluated() const { return _evaluated; }
  ChunkPtr setEvaluated() { _evaluated = true; return this->shared_from_this(); }
  QString show() const;

private:
  ContextPtr _local;
  ExpressionPtr _value;
  bool _evaluated;
};


template <typename T>
inline ChunkPtr::ChunkPtr(const std::shared_ptr<T>& e)
  : Base(new Chunk(e))
{ }

QTextStream& operator<<(QTextStream& ts, const Chunk& tl);

template <typename... T>
ChunkPtr chunk(T&& ...args)
{
  return std::make_shared<Chunk>(std::forward<T>(args)...);
}

ChunkPtr chunk(ChunkPtr c, ContextPtr local);

class Expression : public std::enable_shared_from_this<Expression>
{
public:
  virtual ExpressionPtr copy() const = 0;
  virtual ~Expression() { }
  virtual QString show(bool needBracket = true) const = 0;

  virtual ChunkPtr eval(Interpreter& interpreter, const ContextPtr& local);
};

#define EXPRESSION_COPY(cls) \
  std::shared_ptr<Expression> copy() const override \
  { return std::make_shared<cls>(*this); }

QTextStream& operator<<(QTextStream& ts, const Expression& tl);

ContextPtr makeLocalContext(ChunkPtr l);

class Define : public TopLevel
{
public:
  Define(const QString& name, const ExpressionPtr& value);

  Define(const Define&) = default;
  Define(Define&&) = default;

  QString name() const { return _name; }
  ExpressionPtr value() const { return _value; }

  QString show() const override;
  ExpressionPtr eval(Interpreter& interpreter) const override;

  TOP_LEVEL_COPY(Define)

private:
  QString _name;
  ExpressionPtr _value;
};

MAKE_PTR(Define, define)

class Expr : public TopLevel
{
public:
  explicit Expr(const ExpressionPtr& expr);
  Expr(const Expr&) = default;
  Expr(Expr&&) = default;

  ExpressionPtr expr() const { return _expr; }

  QString show() const override;
  ExpressionPtr eval(Interpreter& interpreter) const override;

  TOP_LEVEL_COPY(Expr)

private:
  ExpressionPtr _expr;
};

MAKE_PTR(Expr, expr)

class Include : public TopLevel
{
public:
  explicit Include(const QString& filename);
  Include(const Include&) = default;
  Include(Include&&) = default;

  QString filename() const { return _filename; }

  QString show() const override;
  ExpressionPtr eval(Interpreter& interpreter) const override;

  TOP_LEVEL_COPY(Include)

private:
  QString _filename;
};

MAKE_PTR(Include, include)

class Atom : public Expression
{
public:
  explicit Atom(const QString& name);
  Atom(const Atom& copy) = default;
  Atom(Atom&&) = default;

  QString name() const { return _name; }

  QString show(bool needBracket = true) const override;

  ChunkPtr eval(Interpreter& interpreter, const ContextPtr& local) override;

  EXPRESSION_COPY(Atom)

private:
  QString _name;
};

MAKE_PTR(Atom, atom)

class Lambda : public Expression
{
public:
  Lambda(const QString& var, const ChunkPtr& body);
  Lambda(const Lambda& copy) = default;
  Lambda(Lambda&&) = default;

  QString var() const { return _var; }
  ChunkPtr body() const { return _body; }

  QString show(bool needBracket = true) const override;
  QString showInternal() const;

  EXPRESSION_COPY(Lambda)

private:

  QString _var;
  ChunkPtr _body;
};

MAKE_PTR(Lambda, lambda)

LambdaPtr makeLambda(QStringList vars, const ExpressionPtr& body);

class App : public Expression
{
public:
  App(const ChunkPtr& fct, const ChunkPtr& arg);
  App(const App& copy) = default;
  App(App&&) = default;

  ChunkPtr fct() const { return _fct; }
  ChunkPtr arg() const { return _arg; }

  QString show(bool needBracket = true) const override;

  ChunkPtr eval(Interpreter& interpreter, const ContextPtr& local) override;

  EXPRESSION_COPY(App)

private:
  ChunkPtr _fct, _arg;
};

MAKE_PTR(App, app)

struct FailComputation : public std::exception
{
  const char* what() const noexcept override
  {
    return "Evaluated a Fail expression";
  }
};

class Fail : public Expression
{
public:
  QString show(bool needBracket = true) const override;

  ChunkPtr eval(Interpreter& interpreter, const ContextPtr& local) override;

  EXPRESSION_COPY(Fail)
};

MAKE_PTR(Fail, fail)

class Internal : public Expression
{
public:
  Internal(const QString& name);
  Internal(const Internal& copy) = default;
  Internal(Internal&&) = default;

  QString show(bool needBracket = true) const override;

  virtual ChunkPtr eval(Interpreter& interpreter, const ChunkPtr& arg) = 0;

protected:
  QString _name;
};

typedef std::shared_ptr<Internal> InternalPtr;

template <typename T, typename Traits>
std::basic_ostream<T,Traits>& operator<<(std::basic_ostream<T,Traits>& ss, const TopLevel& tl)
{
  ss << tl.show().toLocal8Bit().data();
  return ss;
}

template <typename T, typename Traits>
std::basic_ostream<T,Traits>& operator<<(std::basic_ostream<T,Traits>& ss, const Expression& e)
{
  ss << e.show().toLocal8Bit().data();
  return ss;
}

QString escape_atom(QString str);

#endif // AST_H

