#ifndef CONTEXT_FWD_H
#define CONTEXT_FWD_H

class Context;

typedef std::shared_ptr<Context> ContextPtr;

#endif // CONTEXT_FWD_H

