#include "lexer.h"
#include <iostream>
#include "interpreter.h"

#ifdef WIN32
#  include <io.h>
#  define isatty _isatty
#else
#  include <unistd.h>
#endif

QHash<QString, yy::Parser::token::yytokentype> make_keywords()
{
  auto res = QHash<QString, yy::Parser::token::yytokentype>{};
  res[QString::fromUtf8(u8"λ")] = yy::Parser::token::LAMBDA;
  res[QString::fromUtf8(u8"lambda")] = yy::Parser::token::LAMBDA;
  res[QString::fromUtf8(u8"⊥")] = yy::Parser::token::FAIL;
  res[QString::fromUtf8(u8"fail")] = yy::Parser::token::FAIL;
  res[QString::fromUtf8(u8"define")] = yy::Parser::token::DEFINE;
  res[QString::fromUtf8(u8"#include")] = yy::Parser::token::INCLUDE;
  res[QString::fromUtf8(u8".")] = yy::Parser::token::DOT;
  return res;
}

QSet<QChar> make_delimiters()
{
  auto res = QSet<QChar>{};
  res << '.' << '(' << ')' << ' ' << '\n' << '\t' << '\r';
  return res;
}

QHash<QString, yy::Parser::token::yytokentype> Lexer::keywords = make_keywords();
QSet<QChar> Lexer::delimiters = make_delimiters();

Lexer::Lexer(Interpreter& i, bool ts, bool tp)
  : interpreter(i)
  , trace_lexing(ts)
  , trace_parsing(tp)
  , readBuffer(nullptr)
{

}

Lexer::~Lexer()
{
}

int Lexer::parse(const QString& f)
{
  filename = f;
  int error = scan_begin();
  if(error)
    return error;
  yy::Parser parser(*this);
  parser.set_debug_level(trace_parsing);
  try {
    auto res = parser.parse();
    scan_end();
    return res;
  }
  catch(std::exception ex)
  {
    err << "Error caught exception: " << ex.what() << endl;
    return 1;
  }
}

int Lexer::scan_begin()
{
  filename = QDir::current().relativeFilePath(filename);
  f.setFileName(filename);
  buf_pos = 0;
  buffer.clear();
  if(filename.isEmpty())
  {
    file = "<STDIN>";
    filename = "<STDIN>";
    if(!f.open(stdin, QIODevice::ReadOnly))
    {
      err << "Error, cannot open stdin for reading" << endl;
      return Interpreter::CANNOT_READ_STDIN;
    }
    readBuffer = &Lexer::readLine;
    _interactive = isatty(fileno(stdin));
  }
  else
  {
    file = filename.toLocal8Bit().data();
    if(!f.open(QIODevice::ReadOnly))
    {
      err << "Error trying to open file '" << filename << "': " << f.error() << endl;
      return Interpreter::NO_SUCH_FILE;
    }
    readBuffer = &Lexer::readFixedSize;
    _interactive = false;
  }
  interpreter.setInteractive(_interactive);
  ts.setDevice(&f);
  ts.setCodec("utf-8");
  _newStatement = true;
  buffer = (this->*readBuffer)(1);
  pos.initialize(&file);
  return Interpreter::NO_ERROR;
}

void Lexer::scan_end()
{
  buf_pos = 0;
  buffer.clear();
  ts.setDevice(nullptr);
  f.close();
}

YY_DECL
{
  return lexer.next();
}

bool Lexer::atEnd(int offset)
{
  return buf_pos+offset >= buffer.size() and ts.atEnd();
}

bool Lexer::advance()
{
  ++buf_pos;
  if(buf_pos >= buffer.size())
  {
    buffer = (this->*readBuffer)(1);
    buf_pos = 0;
  }
  if(not buffer.isEmpty() and buffer[buf_pos] == '\n') pos.lines(1);
  else pos.columns(1);
  return not atEnd();
}

QString Lexer::readFixedSize(qint64 min)
{
  qint64 to_read = std::max(qint64(1024), min);
  return ts.read(to_read);
}

void Lexer::newStatement()
{
  if(_interactive)
  {
    _newStatement = buf_pos == buffer.size() or (buf_pos == buffer.size()-1 and buffer[buf_pos] == '\n');
  }
}

QString Lexer::readLine(qint64 min)
{
  qint64 s = 0;
  QString new_buffer;
  while(s < min)
  {
    if(_interactive)
    {
      if(_newStatement)
        out << ">>> " << flush;
      else
        out << "... " << flush;
      _newStatement = false;
    }
    auto l = ts.readLine();
    if(l.isNull())
      return new_buffer;
    new_buffer.push_back(l + "\n");
    s += l.size();
  }
  return new_buffer;
}

QChar Lexer::currentChar() const
{
  return buffer[buf_pos];
}

QChar Lexer::lookAhead(int n)
{
  if(buf_pos+n >= buffer.size())
  {
    auto new_buffer = (this->*readBuffer)(buf_pos + n - buffer.size());
    buffer = buffer.mid(buf_pos) + new_buffer;
    buf_pos = 0;
    if(atEnd(n))
      return QChar();
  }
  return buffer[buf_pos+n];
}

QString Lexer::parseString()
{
  auto atom = QString();
  while(true)
  {
    if(!advance())
    {
      errorMessage = "end of file before the end of the string";
      return QString();
    }
    auto c = currentChar();
    if(c == '\\')
    {
      if(!advance())
      {
        errorMessage = "end of file before the end of the string";
        return QString();
      }
      c = currentChar();
      if(c == 'n')
        atom.push_back('\n');
      else if(c == 't')
        atom.push_back('\t');
      else if(c == 'r')
        atom.push_back('\r');
      else if(c == '\n') // ignore the new line
        continue;
      else
        atom.push_back(c);
    }
    else if(c == '"')
    {
      advance();
      return atom;
    }
    else
    {
      atom.push_back(c);
    }
  }
  return atom;
}

QString Lexer::parseAtom()
{
  auto atom = QString();
  do
  {
    auto c = currentChar();
    if(delimiters.contains(c))
      return atom;
    if(c == '{' and lookAhead() == '-')
      return atom;
    else if(c == '-' and lookAhead() == '-')
      return atom;
    atom.push_back(c);
  }
  while(advance());
  return atom;
}

void Lexer::movePosition(const QStringRef& str)
{
  int count_lines = 0, last_idx = 0, idx = 0;
  while((idx = str.indexOf("\n", last_idx+1)) != -1)
  {
    ++count_lines;
    last_idx = idx;
  }
  loc.lines(count_lines);
  loc.columns(str.size() - last_idx);
}

bool Lexer::skip(QString toEnd)
{
  auto idx = buffer.indexOf(toEnd, buf_pos);
  if(idx == -1)
  {
    QString new_buffer = (this->*readBuffer)(1);
    if(new_buffer.isEmpty())
      return false;
    new_buffer.prepend(buffer.right(toEnd.size()));
    // Count the number of lines and columns in the skiped part
    movePosition(buffer.midRef(buf_pos));
    buffer = new_buffer;
    buf_pos = 0;
    return skip(toEnd);
  }
  movePosition(buffer.midRef(buf_pos, idx - buf_pos+toEnd.size()));
  buf_pos = idx + toEnd.size();
  return true;
}

bool Lexer::skipSpaces()
{
  if(atEnd()) return false;
  // Skip spaces
  auto c = QChar();
  while((c = currentChar()).isSpace())
  {
    if(!advance())
      return false;
  }
  if(c == '-' and lookAhead() == '-')
  {
    // Find the next end of line
    advance(); advance();
    if(!skip("\n"))
      return false;
    return skipSpaces(); // skip more
  }
  if(c == '{' and lookAhead() == '-')
  {
    advance(); advance();
    if(!skip("-}"))
      return false;
    // find the end of the comment
    /*
     *auto idx = buffer.indexOf("-}", buf_pos);
     *if(idx == -1)
     *{
     *  err << "Error, end of file before end of comment starting on position " << pos << endl;
     *  return false;
     *}
     *idx += 2;
     *QString com = buffer.mid(buf_pos, idx - buf_pos);
     *auto nb_lines = com.count('\n');
     *if(nb_lines > 0)
     *{
     *  pos.lines(nb_lines);
     *  auto last_newline = com.lastIndexOf('\n');
     *  pos.columns(com.size() - last_newline);
     *}
     *else
     *{
     *  pos.columns(idx - buf_pos);
     *}
     *buf_pos = idx;
     */
    return skipSpaces();
  }
  return true;
}

yy::Parser::symbol_type Lexer::makeAtom(const QString& atom) const
{
  auto found = keywords.value(atom, yy::Parser::token::END);
  if(found != yy::Parser::token::END)
  {
    if(trace_lexing)
      err << "Found keyword '" << atom << "' at loc " << loc << endl;
    return yy::Parser::symbol_type(found, loc);
  }
  if(trace_lexing)
    err << "Found ATOM " << escape_atom(atom) << " at loc " << loc << endl;
  return yy::Parser::make_ATOM(atom, loc);
}

yy::Parser::symbol_type Lexer::next()
{
  loc.begin = loc.end = pos;
  if(!skipSpaces())
    return yy::Parser::make_END(loc);
  // Handle comments
  loc.begin = pos;
  if(currentChar() == '"') // this is a string
  {
    auto atom = parseString();
    if(atom.isEmpty())
    {
      err << "Error while parsing a string: " << errorMessage << endl;
      return yy::Parser::make_END(loc);
    }
    loc.end = pos;
    return makeAtom(atom);
  }
  else if(currentChar() == '(')
  {
    advance(); loc.end = pos;
    if(trace_lexing)
      err << "Found OPEN at loc " << loc << endl;
    return yy::Parser::make_OPEN(loc);
  }
  else if(currentChar() == ')')
  {
    advance(); loc.end = pos;
    if(trace_lexing)
      err << "Found CLOSE at loc " << loc << endl;
    return yy::Parser::make_CLOSE(loc);
  }
  else if(currentChar() == '.')
  {
    advance(); loc.end = pos;
    if(trace_lexing)
      err << "Found DOT at loc " << loc << endl;
    return yy::Parser::make_DOT(loc);
  }
  else
  {
    auto atom = parseAtom();
    loc.end = pos;
    if(atom.isEmpty())
    {
      err << "Error while parsing atom: " << errorMessage << endl;
      return yy::Parser::make_END(loc);
    }
    return makeAtom(atom);
  }
}

void Lexer::error(const yy::location& l, const std::string& m)
{
  std::cerr << l << ": " << m << std::endl;
}

void Lexer::error(const std::string& m)
{
  std::cerr << m << std::endl;
}

